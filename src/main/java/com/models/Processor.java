package main.java.com.models;

import java.util.Objects;

public abstract class Processor {
    int frequency;
    int cache;
    int bitCapacity;

    public Processor(int frequency, int cache, int bitCapacity) {
        this.frequency = frequency;
        this.cache = cache;
        this.bitCapacity = bitCapacity;
    }

    public String getDetails() {
        String datails = new String();
        datails.concat(String.valueOf(frequency))
                .concat("\n")
                .concat(String.valueOf(cache))
                .concat("\n")
                .concat(String.valueOf(bitCapacity));
        return datails;
    }
    public abstract String dataProcess();
    public abstract String dataProcess(long data);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Processor)) return false;
        Processor processor = (Processor) o;
        return frequency == processor.frequency && cache == processor.cache && bitCapacity == processor.bitCapacity;
    }

    @Override
    public int hashCode() {
        return Objects.hash(frequency, cache, bitCapacity);
    }

}


