package main.java.com.models;

public class ProcessorArm extends Processor {
     static final String ARCHITECTURE = "ARM";

    public ProcessorArm(int frequency, int cache, int bitCapacity) {
        super(frequency, cache, bitCapacity);
    }


    @Override
    public String dataProcess() {
        return ARCHITECTURE.toUpperCase();
    }

    @Override
    public String dataProcess(long data) {
        return null;
    }

    public String getProc() {
        return ARCHITECTURE;
    }

    @Override
    public String toString() {
        return "ProcessorArm{" +
                "frequency=" + frequency +
                ", cache=" + cache +
                ", bitCapacity=" + bitCapacity +
                '}';
    }
}
