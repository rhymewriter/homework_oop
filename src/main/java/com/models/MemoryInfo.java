package main.java.com.models;

import main.java.com.servises.Memory;

public class MemoryInfo {
    private int memoryCost;
    private int percentOccMemSpace;

    public MemoryInfo(int memoryCost, int percentOccMemSpace) {
        this.memoryCost = memoryCost;
        this.percentOccMemSpace = percentOccMemSpace;
    }

    @Override
    public String toString() {
        return "MemoryInfo{" +
                "memoryCost=" + memoryCost +
                ", percentOccMemSpace='" + percentOccMemSpace +  "%" + '\'' +
                '}';
    }

    public int getMemoryCost() {
        return memoryCost;
    }

    public void setMemoryCost(int memoryCost) {
        this.memoryCost = memoryCost;
    }

    public int getPercentOccMemSpace() {
        return percentOccMemSpace;
    }

    public void setPercentOccMemSpace(int percentOccMemSpace) {
        this.percentOccMemSpace = percentOccMemSpace;
    }
}
