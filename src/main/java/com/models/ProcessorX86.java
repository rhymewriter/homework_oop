package main.java.com.models;

public class ProcessorX86 extends Processor {
     static final String ARCHITECTURE = "X86";

    public ProcessorX86(int frequency, int cache, int bitCapacity) {
        super(frequency, cache, bitCapacity);
    }

    @Override
    public String dataProcess() {
        return ARCHITECTURE.toLowerCase();
    }

    @Override
    public String dataProcess(long data) {
        return null;
    }

    public String getProc() {
        return ARCHITECTURE;
    }

    @Override
    public String toString() {
        return "ProcessorX86{" +
                "frequency=" + frequency +
                ", cache=" + cache +
                ", bitCapacity=" + bitCapacity +
                '}';
    }
}
