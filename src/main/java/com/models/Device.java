package main.java.com.models;

import main.java.com.servises.Memory;

public class Device {
    private Processor processor;
    private Memory memory;

    public Device(Processor processor, Memory memory) {
        this.processor = processor;
        this.memory = memory;
    }

    public void save(String[] data) {
        if (memory != null) {
            for (String val : data) {
                if (!memory.save(val)) {
                    return;
                }
            }
        }
    }

    public String[] readAll() {
        if (memory != null && memory.getMemoryCell() != null) {
            String[] data = new String[memory.getMemoryCell().length];
            for (int i = 0; i < data.length; i++) {
                if (memory.readLast() != null) {
                    data[i] = memory.readLast();
                    memory.removeLast();
                }

            }
            return data;
        }
        return new String[0];
    }

    public String getSystemInfo() {
        if (memory != null && processor != null) {
            return processor.getDetails()
                    .concat("\n")
                    .concat(memory.getMemoryInfo().toString());
        }
        return "";

    }

    public void dataProcessing() {
        if (memory != null && memory.getMemoryCell() != null) {
            for (int i = 0; i < memory.getMemoryCell().length; i++) {
                if (memory.getMemoryCell()[i] != null) {
                    memory.getMemoryCell()[i] = memory.getMemoryCell()[i].toUpperCase();
                }
            }

        }

    }


    public Processor getProcessor() {
        return processor;
    }

    public Memory getMemory() {
        return memory;
    }

    @Override
    public String toString() {
        return "Device{" +
                "processor=" + processor +
                ", memory=" + memory +
                '}';
    }
}

