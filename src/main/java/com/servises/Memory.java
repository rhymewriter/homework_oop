package main.java.com.servises;

import main.java.com.models.MemoryInfo;

import java.util.Arrays;

public class Memory {
    private String[] memoryCell;

    public Memory() {
    }

    public Memory(int size) {
        this.memoryCell = new String[size];
    }

    public String readLast() {
        if (memoryCell != null) {
            for (int i = memoryCell.length - 1; i > 0; i--) {
                if (memoryCell[i] != null) {
                    return memoryCell[i];
                }

            }
        }
        return null;
    }

    public String removeLast() {
        if (memoryCell != null) {
            for (int i = memoryCell.length - 1; i > 0; i--) {
                if (memoryCell[i] != null) {
                    String tmp = memoryCell[i];
                    memoryCell[i] = null;
                    return tmp;
                }
            }

        }
        return null;
    }

    public boolean save(String value) {
        if (memoryCell != null) {
            if (memoryCell[memoryCell.length - 1] != null) {
                return false;
            }
            for (int i = 0; i < memoryCell.length; i++) {
                if (memoryCell[i] == null) {
                    memoryCell[i] = value;
                    return true;
                }

            }

        }
        return false;
    }

    public MemoryInfo getMemoryInfo() {
        if (memoryCell != null) {
            int numbers;
            int percent;
            for (int i = memoryCell.length - 1; i > 0; i--) {
                if (memoryCell[i] != null) {
                    numbers = memoryCell.length - i;
                    percent = (i * 100) / memoryCell.length;
                    MemoryInfo memoryInfo = new MemoryInfo(numbers, percent);
                    return memoryInfo;
                }
            }
        }
        return new MemoryInfo(0, 0);
    }

    public String[] getMemoryCell() {
        return memoryCell;
    }

    @Override
    public String toString() {
        return "Memory{" +
                "memoryCell=" + getMemoryInfo().getMemoryCost() +
                '}';
    }
}

