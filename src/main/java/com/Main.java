package main.java.com;

import main.java.com.models.*;
import main.java.com.servises.Memory;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        Random random = new Random();


        Device[] devices = new Device[10];
        for (int i = 0; i < devices.length; i++) {
            int memoryData = random.nextInt(29) + 2;
            Memory memory = new Memory(30);
            for (int j = 0; j < memoryData; j++) {
                memory.save(String.valueOf(j));
            }

            if (i % 2 == 0) {
                Device device = new Device(new ProcessorArm(1100, 16, 64), memory);
                devices[i] = device;

            } else {
                Device device = new Device(new ProcessorX86(2400, 128, 32), memory);
                devices[i] = device;
            }


        }




        List<Device> arm = Arrays.stream(devices)
                .filter(t -> t.getProcessor().dataProcess().equalsIgnoreCase("arm"))
                .collect(Collectors.toList());
        System.out.println(arm.toString());

        List<Device> x86 = Arrays.stream(devices)
                .filter(t -> t.getProcessor().dataProcess().equalsIgnoreCase("x86"))
                .collect(Collectors.toList());
        System.out.println(x86.toString());

        List<Device> more = Arrays.stream(devices)
                .filter(t -> t.getMemory().getMemoryInfo().getMemoryCost() > 5)
                .collect(Collectors.toList());
        System.out.println(more.toString());

        List<Device> percentValue = Arrays.stream(devices)
                .filter(t -> t.getMemory().getMemoryInfo().getPercentOccMemSpace() < 30)
                .collect(Collectors.toList());
        System.out.println(percentValue.toString());



    }
}
